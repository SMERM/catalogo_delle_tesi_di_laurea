# Davide Tedesco - The Sustainability and Preservation of Electro-Acoustic Music: From the Process to the Abstraction

The thesis and all the presented materials can be found [here](https://gitlab.com/SMERM/bn-tesi-tedesco)!
