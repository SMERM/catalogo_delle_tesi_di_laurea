# Catalogo delle Tesi di Laurea

Catalogo delle Tesi di Laurea della Scuola di Musica Elettronica del Conservatorio di Musica di Roma, Santa Cecilia.

Tutti i materiali qui reperibili sono stati presentati in sede di Diploma presso il la Scuola di Musica Elettronica del Conservatorio di Musica di Roma, Santa Cecilia.

## Come caricare la propria tesi?

1. aggiungere una cartella nell'anno accademico desiderato con:
  - corso di studi (TR o BN)
  - nome del diplomato per esteso
2. Inserire i file con nome, cognome e titolo all'interno della cartella:
  - tesi
  - file audio
  - partiture

### Esempio
Nome della cartella:
```
TR_Nome_Cognome
```
Nome dei files all'interno della cartella:
```
Nome_Cognome_Nome_della_tesi.pdf
Nome_Cognome_Nome_della_tesi.tex
Nome_Cognome_Nome_del_brano.pdf
Nome_Cognome_Nome_del_brano.ly
Nome_Cognome_Nome_del_brano.flac
```

_Sono preferiti i files .pdf e .tex per le tesi, i files .pdf e .ly per le partiture ed i files .flac per i files audio._
